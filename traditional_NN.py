import numpy as np
import os
import gzip
import time
# Killing optional CPU driver warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

training_number = 10000
testing_number = 10000

class Model:
    """
        Arguments:
        train_images - NumPy array of training images
        train_labels - NumPy array of labels
    """
    def __init__(self, train_images, train_labels):
        self.input_size, self.num_classes, self.batchSz, self.learning_rate = 784, 10, 1, 0.5
        self.train_images = train_images
        self.train_labels = train_labels
        # sets up weights and biases...
        # self.W = _, there are 7840 weights here, treat it as a 784 * 10 matrix
        self.W = np.random.random((self.input_size, self.num_classes)) #np.zeros((self.input_size, self.num_classes), float)
        # self.b = _, there are 10 biases here, from b0 to b9. Treat it as a 1*10 matrix
        self.b = np.random.random((1, self.num_classes)) #np.zeros((1, self.num_classes), float)

    def run(self, image_index):
        """
        Does the forward pass, loss calculation, and back propagation
        for this model for one step

        Args:
        image_index: ith image from training data, i from 0 to 10E3 - 1
        Return: None
        """
        '''-------- first do the forward pass ---------'''
        # logits (array of 1x10) = biases (array of 1x10) +
        # scaled_inputs (array of 1x784) * weights (array of 784x10)
        input_set = self.train_images[image_index * self.input_size: (image_index * self.input_size + self.input_size)]
        inputs_scaled = input_set / 255  # this is normalized input for each training set
        label_set = self.train_labels[image_index]

        logits = self.b + np.dot(inputs_scaled, self.W)

        # calculate probability
        logits_list = logits.tolist()[0]
        logits_exp_list = [np.exp(x) for x in logits_list]
        prob_denom = sum(logits_exp_list)
        prob_list = [x/prob_denom for x in logits_exp_list]

        '''-------- now do backward pass --------'''
        # update bias (1x10 array)
        delta_b = []
        for index in range(0, len(prob_list)):
            if label_set == index:
                delta = self.learning_rate * (1 - prob_list[index])
                delta_b.append(delta)
            else:
                delta = -1 * self.learning_rate * prob_list[index]
                delta_b.append(delta)

        delta_b_array = np.asarray(delta_b)
        self.b = self.b + delta_b_array

        # update weights (784x10 array)
        delta_w = np.zeros((self.input_size, self.num_classes))
        for column in range(0, self.num_classes):  # 10 columns representing neurons
            for row in range(0, self.input_size):  # 784 rows representing inputs
                if label_set == column:
                    delta = self.learning_rate * (1 - prob_list[column]) * inputs_scaled[row]
                    delta_w[row][column] = delta
                else:
                    delta = -1 * self.learning_rate * prob_list[column] * inputs_scaled[row]
                    delta_w[row][column] = delta

        self.W = self.W + delta_w

        pass


    def test(self, test_images, test_labels):
        input_scaled = test_images / 255

        pass_num = 0

        for test_index in range(0, testing_number):
            test_input_set = input_scaled[test_index * 784: (test_index * 784 + 784)]
            test_label_set = test_labels[test_index]

            '''------ do forward propagation ------ '''
            test_logits = self.b + np.dot(test_input_set, self.W)

            # calculate probability
            test_logits_list = test_logits.tolist()[0]
            test_logits_exp_list = [np.exp(x) for x in test_logits_list]
            test_prob_denom = sum(test_logits_exp_list)
            test_prob_list = [x / test_prob_denom for x in test_logits_exp_list]

            # compare the NN largest probability label to the test label
            NN_decision = np.argmax(test_prob_list)
            if NN_decision == test_label_set:
                pass_num = pass_num + 1

            pass
        return pass_num / testing_number * 100
        pass



def main():
    # TO-DO: import MNIST test data
    with open('train-images-idx3-ubyte.gz', 'rb') as f, gzip.GzipFile(fileobj=f) as bytestream:
        byteread_header = bytestream.read(16)
        byteread_image = bytestream.read(784 * training_number)#(784 * 10E3)  # read in 10000 images as training data

        print("reading training image file")

    with open('train-labels-idx1-ubyte.gz', 'rb') as f, gzip.GzipFile(fileobj=f) as bytestream:
        byteread_header = bytestream.read(8)
        byteread_label = bytestream.read(1 * training_number)  # read in 10000 labels as training data

        print("reading training label file")

    with open('t10k-images-idx3-ubyte.gz', 'rb') as f, gzip.GzipFile(fileobj=f) as bytestream:
        byteread_header = bytestream.read(16)
        byteread_image_test = bytestream.read(784 * testing_number)  # read in 10000 images as testing data

        print("reading testing image file")

    with open('t10k-labels-idx1-ubyte.gz', 'rb') as f, gzip.GzipFile(fileobj=f) as bytestream:
        byteread_header = bytestream.read(8)
        byteread_label_test = bytestream.read(1 * testing_number)  # read in 10000 labels as training data

        print("reading testing label file")

    train_image = np.frombuffer(byteread_image, np.uint8)
    train_label = np.frombuffer(byteread_label, np.uint8)
    test_image = np.frombuffer(byteread_image_test, np.uint8)
    test_label = np.frombuffer(byteread_label_test, np.uint8)

    network = Model(train_image, train_label)
    print("network initialized")

    print("training started")

    start_time = time.time()

    for training_index in range(0, training_number):
        network.run(training_index)
        if ((training_index / training_number) * 10) % 1 == 0:
            print("\r", (training_index / training_number * 100), "% done", sep = "", end = "")

    training_time = time.time() - start_time

    print("\rtraining ended")
    print("training time is ", str(training_time), "s", sep = "")

    amax = max(max(x) for x in network.W)
    amin = min(min(x) for x in network.W)
    print("testing started")

    start_time = time.time()

    pass_rate = network.test(test_image, test_label)

    testing_time = time.time() - start_time

    print("pass rate is ", str(pass_rate), "%", sep = "")
    print("testing time is ", str(testing_time), "s", sep = "")

    pass

if __name__ == '__main__':
    main()
